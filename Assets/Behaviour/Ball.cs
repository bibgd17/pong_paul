﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

	bool direc = false;
	bool go = true;
	public float ymod;
	public float speed;
	public float BallRange;
	public float modrange;
	public float paddlesize;
	public float deadzone;
	public float PaddlePos;
	public KeyCode ResetButton = KeyCode.Space;
    private GameObject Score_A;
    private GameObject Score_B;
	void Start () {
        Score_A = GameObject.Find("Score_A");
        Score_B = GameObject.Find("Score_B");
    }
	void BallHit(string Stickname, bool side){
		GameObject Stick = GameObject.Find(Stickname);
		float by = transform.position.y;
		float sy = Stick.transform.position.y;
		if(by >= sy + deadzone && by <= sy + paddlesize){
			direc = side;
			ymod = by-sy;
		}else if(by <= sy - deadzone && by >= sy - paddlesize){
			direc = side;
			ymod = by-sy;
		} else if(by >= sy - deadzone && by <= sy + deadzone){
			direc = side;
			ymod = 0.0F;
		} else {
			ResetPos(side);
		}
	}
	void ResetPos(bool side){
		transform.position = new Vector3(0, 0);
		ymod = 0.0F;
		speed = 0.01F;
		go = false;
        if (side == false)
        {
            Score_A.GetComponent<Score>().increment();
            Debug.Log("Hitted A");
        }
        if (side == true)
        {
            Score_B.GetComponent<Score>().increment();
            Debug.Log("Hitted B");
        }
    }
	void Move (float value) { //Movement Void;
		if(ymod >= modrange){ //Limit Angle
			ymod = modrange;
		}
		if(ymod <= -modrange){
			ymod = -modrange;
		}
		transform.position = new Vector3(transform.position.x + (value), transform.position.y + ymod); //change position by Vector3
		if(transform.position.y <= -BallRange || transform.position.y >= BallRange){ //On wallhit
			ymod = -ymod;
		}
	}
	// Update is called once per frame
	void Update () {
		if(transform.position.x <= -PaddlePos){
			BallHit("Paddle_A", true);
		}
		if(transform.position.x >= PaddlePos){
			BallHit("Paddle_B", false);
		}
		if(Input.GetKey(ResetButton)){
			go = true;
		}
		if(go == true){
			if(direc == false){
				Move(-0.1F+ speed);
			}
			if(direc == true){
				Move(0.1F + speed);
			}
		}
	}
}
