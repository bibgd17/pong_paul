﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour {

    public KeyCode UpKey;
    public KeyCode DownKey;
	public float speed;
    public float range;

    void Move (float value) {
		transform.position = new Vector3(transform.position.x, transform.position.y + value * Time.deltaTime);
	}
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(transform.position.y <= range)
        {
            if (Input.GetKey(UpKey))
            {
                Move(speed);
            }
        } //End Range up IF
        if(transform.position.y >= -range)
        {
            if (Input.GetKey(DownKey))
            {
                Move(-speed);
            }
        } //End Range down IF
	} //End Update loop
}
